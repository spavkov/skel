#!/bin/sh
# Create a skeleton based off of a template directory
# Usage:
# skel -l
# skel path/to/project

skelDir="${HOME}/doc/git/skel"
projDir="$1"
[ -d $skelDir ] ||  exit 1 # exit if direcotry doesn't exist
skels="$(find $skelDir -type d -name 's_*')"

# exit if ran without arguments
[ -z "$projDir" ] && { echo "Need a directory or \"-l\" to list." >&2 ; exit 3 ; }

# list skeletons
[ "$projDir" = "-l" ] && exec find $skelDir -maxdepth 1 -type d -name 's_*'

clean_skel() {
	project="$1"

	cd $project
	find . -name '.gitkeep' -delete

	# get list of s_ files
	skelFiles=$(find $skelDir -name 's_*')

	# replace s_project with the project name in all s_ files
	sed "s/s_project/${project##*/}/" -i $skelFiles

	# rename s_ files to their proper name
	rename 's_' '' -v $skelFiles
}

make_skel() { # wrapper
	skeleton="$1"
	# remove any traling slashes from project path
	project="${2%/}"

	# if directory already exists, then exit
	[ -e "$project" ] && { echo "$project already exists" >&2 ; exit 5 ; }

	# if directory does not exist, create it
	[ ! -e "$(dirname ${project%/*})" ] && mkdir -p "$(dirname ${project%/*})"
	cp -r $skeleton $project # otherwise copy files normally
	clean_skel $project
}

# select skeleton
skel="$(echo "${skels}" | tr ' ' '\n' | fzy)"
[ -n "$skel" ] && skel="${skelDir}/$(basename $skel)" || exit 7

[ -d "$skel" ] && make_skel $skel $projDir || exit 2
