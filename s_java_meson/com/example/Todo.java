/**
 * Hello World Example
 *
 * @author Spencer Pavkovic
 *
 */
public class Todo
{
	/**
	 * @param args
	 */
	public static void main (String[] args)
	{
		System.out.println("Hello, World!");
	}
}
